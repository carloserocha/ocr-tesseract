from PIL import Image
from re import findall
import pytesseract as ocr
import numpy as np
import cv2 as cv
from os import environ

ocr.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
# environ['TESSDATA_PREFIX'] = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
# tipando a leitura para os canais de ordem RGB
imagem = Image.open('imgs/lulinha.jpg').convert('RGB')

# convertendo em um array editável de numpy[x, y, CANALS]
npimagem = np.asarray(imagem).astype(np.uint8)

# diminuição dos ruidos antes da binarização
npimagem[:, :, 0] = 0 # zerando o canal R (RED)
npimagem[:, :, 2] = 0 # zerando o canal B (BLUE)

# atribuição em escala de cinza
im = cv.cvtColor(npimagem, cv.COLOR_RGB2GRAY)

# aplicação da truncagem binária para a intensidade
# pixels de intensidade de cor abaixo de 127 serão convertidos para 0 (PRETO)
# pixels de intensidade de cor acima de 127 serão convertidos para 255 (BRANCO)
# A atrubição do THRESH_OTSU incrementa uma análise inteligente dos nivels de truncagem
ret, thresh = cv.threshold(im, 127, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)

# reconvertendo o retorno do threshold em um objeto do tipo PIL.Image
binimagem = Image.fromarray(thresh)

'''

pytesseract.image_to_string(Image.open("./imagesStackoverflow/xyz-small-gray.png"),
                                  lang="eng",boxes=False,
                                  config="--psm 4 --oem 3 
                                  -c tessedit_char_whitelist=-01234567890XYZ:"))
                                  '''

phrase = ocr.image_to_string(binimagem, lang='eng', config='--psm 6 --oem 3')

print(phrase)

phrase = findall(r'[a-zA-z]{2,}', phrase)

print(phrase)